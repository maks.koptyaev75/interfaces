package com.example.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //TextView tv;
    //CheckBox chb;
    TextView tvColor, tvSize;
    final int MENU_COLOR_RED = 1;
    final int MENU_COLOR_GREEN = 2;
    final int MENU_COLOR_BLUE = 3;

    final int MENU_SIZE_22 = 4;
    final int MENU_SIZE_26 = 5;
    final int MENU_SIZE_30 = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvColor = (TextView) findViewById(R.id.tvColor);
        tvSize = (TextView) findViewById(R.id.tvSize);
        registerForContextMenu(tvColor);
        registerForContextMenu(tvSize);

        //tv = (TextView) findViewById(R.id.textView);
        //chb = (CheckBox) findViewById(R.id.chbExtMenu);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        switch (v.getId()){
            case R.id.tvColor:
                menu.add(0, MENU_COLOR_RED, 0, "Red");
                menu.add(0, MENU_COLOR_GREEN, 0, "Green");
                menu.add(0, MENU_COLOR_BLUE, 0, "Blue");
                break;
            case R.id.tvSize:
                menu.add(0, MENU_SIZE_22, 0, "22");
                menu.add(0, MENU_SIZE_26, 0, "26");
                menu.add(0, MENU_SIZE_30, 0, "30");
                break;

        }
    }

    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case MENU_COLOR_RED:
                tvColor.setTextColor(Color.RED);
                tvColor.setText("Text color = red");
                break;
            case MENU_COLOR_GREEN:
                tvColor.setTextColor(Color.GREEN);
                tvColor.setText("Text color = green");
                break;
            case MENU_COLOR_BLUE:
                tvColor.setTextColor(Color.BLUE);
                tvColor.setText("Text color = blue");
                break;
            case MENU_SIZE_22:
                tvSize.setTextSize(22);
                tvSize.setText("Text size = 22");
                break;
            case MENU_SIZE_26:
                tvSize.setTextSize(26);
                tvSize.setText("Text size = 26");
                break;
            case MENU_SIZE_30:
                tvSize.setTextSize(30);
                tvSize.setText("Text size = 30");
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void registerForContextMenu(View view){
        view.setOnCreateContextMenuListener(this);
    }

    //public boolean onCreateOptionsMenu(Menu menu){
        //menu.add(0, 1, 0, "add");
        //menu.add(0, 2, 0, "edit");
        //menu.add(0, 3, 3, "delete");
        //menu.add(1, 4, 1, "copy");
        //menu.add(1, 5, 2, "paste");
        //menu.add(1, 6, 4, "exit");
        //return super.onCreateOptionsMenu(menu);
    //}

    //public boolean onPrepareOptionalsMenu(Menu menu){
        //menu.setGroupVisible(1, chb.isChecked());
        //return super.onPrepareOptionsMenu(menu);
    //}

    //public boolean onOptionalsItemSelected(MenuItem item){
        //StringBuilder sb = new StringBuilder();
        //sb.append("Item Menu");
        //sb.append("\r\n groupId: " + String.valueOf(item.getGroupId()));
        //sb.append("\r\n itemId: " + String.valueOf(item.getItemId()));
        //sb.append("\r\n order: " + String.valueOf(item.getOrder()));
        //sb.append("\r\n title: " + item.getTitle());
        //tv.setText(sb.toString());
        //return super.onOptionsItemSelected(item);
    //}
}