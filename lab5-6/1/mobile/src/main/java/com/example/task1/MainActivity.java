package com.example.task1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int rInt;
    int vvodInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button save = findViewById(R.id.b);
        EditText vvod = findViewById(R.id.eT);
        TextView otvet = findViewById(R.id.t);
        Random random = new Random();
        rInt = random.nextInt(100);
        rInt++;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vvodInt=Integer.parseInt(vvod.getText().toString());
                if(rInt > vvodInt){
                    otvet.setText("Число "+vvodInt+" менше загаданого");
                }
                else if(rInt < vvodInt){
                    otvet.setText("Число "+vvodInt+" більше загаданого");
                }
                else if(rInt == vvodInt){
                    otvet.setText("Ви вгадали!");
                }
            }
        });

    }
}